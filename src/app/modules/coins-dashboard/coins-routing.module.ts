import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableViewComponent } from './table-view/table-view.component';
import { ChartViewComponent } from './chart-view/chart-view.component';
import { CHART_VIEW, TABLE_VIEW } from './coins-module-routes';

const routes: Routes = [
  {
    path: '',
    redirectTo: TABLE_VIEW,
    pathMatch: 'full',
  },
  {
    path: TABLE_VIEW,
    component: TableViewComponent,
  },
  {
    path: CHART_VIEW,
    component: ChartViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoinsRoutingModule {}
