import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject, of, from, forkJoin, combineLatest } from 'rxjs';
import { ICoin } from '@interfaces/coin';
import { UpdateCoinState } from '@store/coins-list-action';
import { takeUntil, map, mergeMap } from 'rxjs/operators';
import { HttpService } from '@services/http.service';
import { SetLoader } from '@store/loader-action';
import { ICoinHistory } from '@interfaces/coin-history';
import { PushToCoinsHistoryList, RemoveFromHistoryList } from '@store/coins-history-action';
import { ICoinAnalitics } from '@interfaces/coin-analitics';
import { PushToCoinsAnaliticsList, RemoveFromAnaliticsList } from '@store/coins-analitics-action';

@Component({
  selector: 'app-coins-list',
  templateUrl: './coins-list.component.html',
  styleUrls: ['./coins-list.component.scss'],
})
export class CoinsListComponent implements OnInit {
  public coinsList: Observable<ICoin[]>;

  constructor(private store: Store<any>, private http: HttpService) {}

  ngOnInit() {
    this.coinsList = this.store.select('coinsList');
  }
  setActive(index: number, coin: ICoin): void {
    this.store.dispatch(new UpdateCoinState(index));
    if (coin.active) {
      const coinHistory = this.http.getCoinHistory(coin.id);
      const coinAnalitics = this.http.getCoinAnalitics(coin.id);
      combineLatest(coinHistory, coinAnalitics).subscribe(([coinHistory, coinAnalitics]) => {
        const coinHistoryItem: ICoinHistory = {
          id: coin.id,
          color: coin.color || '#BBC2D2',
          history: coinHistory.data.history,
        };
        const coinAnaliticItem: ICoinAnalitics = {
          id: coin.id,
          name: coin.name,
          color: coin.color || '#BBC2D2',
          firstSeen: Number(coinAnalitics.data.coin.firstSeen),
          price: Number(coinAnalitics.data.coin.price),
          highestPrice: Number(coinAnalitics.data.coin.allTimeHigh.price),
        };
        this.store.dispatch(new PushToCoinsHistoryList(coinHistoryItem));
        this.store.dispatch(new PushToCoinsAnaliticsList(coinAnaliticItem));
        this.store.dispatch(new SetLoader(false));
      });
    } else {
      this.store.dispatch(new RemoveFromHistoryList(coin.id));
      this.store.dispatch(new RemoveFromAnaliticsList(coin.id));
    }
  }

  ngOnDestroy(): void {}
}
