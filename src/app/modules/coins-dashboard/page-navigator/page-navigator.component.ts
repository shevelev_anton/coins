import { Component, OnInit } from '@angular/core';
import { CoinsRoutes } from '../coins-module-routes';

@Component({
  selector: 'app-page-navigator',
  templateUrl: './page-navigator.component.html',
  styleUrls: ['./page-navigator.component.scss']
})
export class PageNavigatorComponent implements OnInit {

  constructor(public routes: CoinsRoutes) { }

  ngOnInit() {
  }

}
