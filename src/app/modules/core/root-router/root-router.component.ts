import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SomeError } from '@classes/error';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root-router',
  templateUrl: './root-router.component.html',
  styleUrls: ['./root-router.component.scss']
})
export class RootRouterComponent implements OnInit {
  public loader: Observable<boolean> = new Observable();
  public error: Observable<SomeError> = new Observable();
  constructor(public store: Store<any>) {}

  ngOnInit(): void {
    this.loader = this.store.select('loader');
    this.error = this.store.select('error');
  }

}
