import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootRouterComponent } from './root-router.component';

describe('RootRouterComponent', () => {
  let component: RootRouterComponent;
  let fixture: ComponentFixture<RootRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
