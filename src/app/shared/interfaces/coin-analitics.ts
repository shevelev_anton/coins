export interface ICoinAnalitics {
  id: number;
  color: string;
  name: string;
  firstSeen: number;
  price: number;
  highestPrice: number;
}
