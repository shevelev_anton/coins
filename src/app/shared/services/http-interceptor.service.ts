import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';
import { SomeError } from '@classes/error';
import { Store } from '@ngrx/store';
import { SetLoader } from '@store/loader-action';
import { SetError } from '@store/error-action';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  constructor(private store: Store<any>) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    setTimeout(()=>{
      this.store.dispatch(new SetLoader(true));
    },0);
    let modified = request.clone();
    return next.handle(modified).pipe(
      catchError((error: HttpErrorResponse) => {
        let errorMessage = 'something went wrong';
        switch (error.status) {
          case 400: {
            errorMessage = `You've sent bad params.`;
            break;
          }
          case 401: {
            errorMessage = `User unauthorized. Check your login or password`;
            break;
          }
          case 404: {
            errorMessage = `404. No data found`;
            break;
          }
        }
        this.store.dispatch(new SetLoader(false));
        this.store.dispatch(new SetError(new SomeError(errorMessage, true)));
        return throwError(error);
      })
    );
  }
}
