import { ActionReducerMap } from '@ngrx/store';
import { showLoader } from './loader-reducer';
import { showError } from './error-reducer';
import { coinsList } from './coins-list-reducer';
import { coinsHistoryList } from './coins-history-reducer';
import { coinsAnaliticsList } from './coins-analitics-reducer';

export const reducers: ActionReducerMap<any> = {
    loader: showLoader,
    error: showError,
    coinsList: coinsList,
    coinsHistory: coinsHistoryList,
    coinsAnalitics: coinsAnaliticsList,
};
