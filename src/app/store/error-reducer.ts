import { defaultErrorState } from './error-action';
import { ErrorActions } from '@actions';
import { ErrorStateActionType } from '@types';

export function showError(
  state = defaultErrorState,
  action: ErrorStateActionType
) {
  switch (action.type) {
    case ErrorActions.ERROR_STATE:
      return action.payload;
    default:
      return state;
  }
}
