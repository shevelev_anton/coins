import { CoinsHistoryListActionType } from '@types';
import { defaultCoinsHistoryList } from './coins-history-action';
import { CoinsHistoryListActions } from '@actions';
import { ICoinHistory } from '@interfaces/coin-history';

export function coinsHistoryList(
  state = defaultCoinsHistoryList,
  action: CoinsHistoryListActionType,
) {
  switch (action.type) {
    case CoinsHistoryListActions.PUSH_COINS_HISTORY_LIST:
      return Object.assign([], state.concat([action.payload] as any));
    case CoinsHistoryListActions.REMOVE_FROM_COINS_HISTORY_LIST:
      return Object.assign([], state.filter((coinHistoryItem)=>{
        return coinHistoryItem.id !== action.payload
      }));
    default:
      return state;
  }
}
