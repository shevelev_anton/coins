export enum LoaderActions {
  LOADER_STATE = '[loader] STATE',
}

export enum ErrorActions {
  ERROR_STATE = '[error] STATE',
}
export enum CoinListActions {
  COINS_LIST = '[coins] LIST',
  COINS_STATE = '[coins] STATE',
}

export enum CoinsHistoryListActions {
  PUSH_COINS_HISTORY_LIST = '[coins-history-list] PUSH',
  REMOVE_FROM_COINS_HISTORY_LIST = '[coins-history-list] REMOVE',
}
export enum CoinsAnaliticsListActions {
  PUSH_COINS_ANALITICS_LIST = '[coins-analitics-list] PUSH',
  REMOVE_FROM_COINS_ANALITICS_LIST = '[coins-analitics-list] REMOVE',
  SORT_BY = '[coins-analitics-list] SORT_BY',
}
