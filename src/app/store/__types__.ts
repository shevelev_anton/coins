import {
  LoaderActions,
  ErrorActions,
  CoinListActions,
  CoinsHistoryListActions,
  CoinsAnaliticsListActions,
} from '@actions';
import { SomeError } from '@classes/error';
import { ICoin } from '@interfaces/coin';
import { ICoinHistory } from '@interfaces/coin-history';

/**
 * does loader should be displayed
 * @param boolean payload
 */

interface LoaderStateAction {
  type: typeof LoaderActions.LOADER_STATE;
  payload: boolean;
}
export type LoaderStateActionType = LoaderStateAction;

/**
 * does error should be displayed
 * @param SomeError payload
 */

interface ErrorStateAction {
  type: typeof ErrorActions.ERROR_STATE;
  payload: SomeError;
}
export type ErrorStateActionType = ErrorStateAction;

/**
 * does error should be displayed
 * @param Coin payload
 */

interface CoinsListStateAction {
  type: typeof CoinListActions.COINS_LIST;
  payload: ICoin[];
}

interface CoinStateAction {
  type: typeof CoinListActions.COINS_STATE;
  payload: number;
}

export type CoinStateActionType = CoinsListStateAction | CoinStateAction;

interface PushCoinsHistoryListAction {
  type: typeof CoinsHistoryListActions.PUSH_COINS_HISTORY_LIST;
  payload: ICoinHistory[];
}

interface RemoveCoinsHistoryListAction {
  type: typeof CoinsHistoryListActions.REMOVE_FROM_COINS_HISTORY_LIST;
  payload: number;
}

export type CoinsHistoryListActionType = PushCoinsHistoryListAction | RemoveCoinsHistoryListAction;

interface PushCoinsAnaliticsListAction {
  type: typeof CoinsAnaliticsListActions.PUSH_COINS_ANALITICS_LIST;
  payload: ICoinHistory[];
}

interface RemoveCoinsAnaliticsListAction {
  type: typeof CoinsAnaliticsListActions.REMOVE_FROM_COINS_ANALITICS_LIST;
  payload: number;
}
interface SortByCoinsAnaliticsListAction {
  type: typeof CoinsAnaliticsListActions.SORT_BY;
  payload: string;
}

export type CoinsAnaliticsListActionType =
  | PushCoinsAnaliticsListAction
  | RemoveCoinsAnaliticsListAction
  | SortByCoinsAnaliticsListAction;
