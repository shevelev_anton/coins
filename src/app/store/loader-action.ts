import { Action } from '@ngrx/store';
import { LoaderActions } from '@actions';

export const defaultLoaderState = false;

export class SetLoader implements Action {
  readonly type = LoaderActions.LOADER_STATE;
  constructor(public payload: boolean) {}
}
