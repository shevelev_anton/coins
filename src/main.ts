import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


import { environment } from './environments/environment';
import { CoreModule } from './app/modules/core/core.module';

if (environment.production) {
  enableProdMode();
  window.console.log = window.console.warn = window.console.info = function(){};
}

platformBrowserDynamic().bootstrapModule(CoreModule)
  .catch(err => console.error(err));
